#!/bin/bash

#***** Projet 3******

function_creation_user()
{
        sudo adduser $USER 
}
function_suppr_user()
{
	sudo deluser $USER
}

while :
do
	echo "1- Créer un utilisateur"
	echo "2- Supprimer un utilisateur"
	echo "q- Quitter"
	read -p "Votre choix :" N

	case $N in 
		1)
			echo "Veuillez saisir un nom d'utilisateur :" && read -r USER
			VAR=$USER
			cut -d ":" -f 1 /etc/passwd | grep -l "$VAR" > /dev/null
			if [ $? -eq 0 ]
			then 
				echo "Cet utilisateur existe déjà :création impossible !"
			else
				function_creation_user
			fi
			;;
		2)
			echo "Veuillez saisir un nom d'utilisateur à supprimer : " && read -r USER
			VAR=$USER
			cut -d ":" -f 1 /etc/passwd | grep -l "$VAR" > /dev/null 
			if [ $? -eq 0 ] 
			then 
				echo "Etes vous sûr de vouloir supprimer cet utilisateur y/n ?:" && read -r C
				if [ $C = "y" ]
				then 
					function_suppr_user
				else
					exit 0
				fi
			else
				echo "Cet utilisateur n'existe pas :Suppression impossible ! "
			fi
			;;
		q)
			exit 1
			;;
		*) 
			exit 2
			;;
	esac
done




