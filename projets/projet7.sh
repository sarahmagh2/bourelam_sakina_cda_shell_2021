#!/bin/bash

#********* Projet 7*********

echo "Ce script va modifié les droits sur un fichier saisi par l'utilisateur :"
echo "************************************************************************"

echo "Veuillez saisir un nom de fichier : " && read -r F
VAR=$F

if [ -e $F ]
then
	echo "Le fichier existe"
	ls -l
	echo "*************************************"
	echo "Veuillez entrez les droits pour l'utilisateur propriétaire pour ce fichier  :" && read -r DP
	VAR1=$DP
	sudo chmod  $VAR1 $VAR
	echo "Veuillez saisir les droits pour le groupe propriétaire  : " && read -r DG
	VAR2=$DG
	sudo chmod $VAR2 $VAR
	echo "Veuillez sasir les droits pour les autres : " && read -r DA
	VAR3=$DA
	sudo chmod $VAR3 $VAR
else

	echo "Ce fichier n'existe pas "
	exit 0
fi
