#!/bin/bash

#*********Projet 6 *********

echo "Ce script va modifié les droits de propriété sur un fichier choisi:"
echo "*******************************************************************"

echo "Veuillez saisir un nom de fichier :" && read -r FICH
VAR=$FICH
if [ -e $VAR ]
then
	echo "Le fichier existe "
	ls -l 
	echo "***************************************************"
	echo "Quel est le nouveau  utilisateur propriétaire pour ce fichier :" && read -r USER
        
	VAR1=$USER
	sudo chown $VAR1 $VAR
	ls -l 
	echo "*******************************************************"
	echo "Quel est le nouveau groupe propriétaire pour ce fichier : " && read -r GROUP
       	VAR2=$GROUP
	sudo chgrp $VAR2 $VAR 
	ls -l 
	echo "********************************************************"
else
	echo "Ce fichier n'existe pas! :"
	exit 0
fi


