#!/bin/bash

#******Projet 4*******

function_create_group()
{
	sudo addgroup $GROUP
}
function_suppr_group()
{
	sudo delgroup $GROUP
}


while :
do
	echo "1- Créer un groupe"
	echo "2- Supprimer un groupe"
	echo "q- Quitter"
	read -p "Votre choix : " N

	case $N in
	1)
		echo "Veuillez saisir un nom de groupe : " && read -r GROUP
	        VAR=$GROUP
                cut -d ":" -f 1 /etc/group |grep -l "$VAR" > /dev/null
                if [ $? -eq 0 ]
	        then
	        echo "Ce groupe existe déjà"
                echo "**********************"
	else
			function_create_group
		fi
		;;
	2)
		echo "Veuillez saisir un nom de groupe à supprimer :" && read -r GROUP
		VAR=$GROUP
		cut -d ":" -f 1 /etc/group | grep -l "$VAR" > /dev/null
		if [ $? -eq 0 ]
		then
			echo "Etes vous sur de vouloir supprimer ce groupe !: y/n " && read -r C
			if [ $C = "y" ]
			then
				function_suppr_group
			else
				exit 0
			fi
		else
			echo " Ce groupe n'existe pas : Suppression impossible ! "
			echo " *************************************************"
		fi
		;;
	q)
		exit 1
		;;
	*)
		exit 2
		;;
esac
done

			

