#!/bin/bash


#******* Projet 5**********

echo " Ce script va vous guider pour créer ou supprimer un fichier : donc faites votre choix :"
echo " ************************************************************************************ "

function_create_fich()

{ 
	sudo touch $VAR

}

function_suppr_fich()

{
	sudo rm $VAR

}
while :
do
	echo "1- Créer un fichier"
        echo "2- Supprimer un fichier"
        echo "q- Quitter"
	read -p "Votre choix : " N

	case $N in

		1) 
			echo "Veuillez saisir le nom du fichier que vous voulez créer " && read -r FICH
			VAR=$FICH
			if [ -e $VAR ]
			then
			    echo "Le fichier existe déjà !"
		            echo "************************"
		        else
			       function_create_fich
			       ls
			       echo "Le fichier a bien été créé"
			fi
			;;
		2)
	               echo "Veuillez saisir le nom du fichier à supprimer " && read -r FICH
                        
 		       VAR=$FICH
		       if [ -e $VAR ]
		       then 
			       echo "Etes vous sûr de bien vouloir supprimer ce fichier : y/n ?" && read R
			       
			       if [ $R = "y" ]
			       then 
				       function_suppr_fich
				       ls
				       echo "Le fichier a bien été supprimé !"
			       else
				       exit 0
			       fi
		       else
			       echo "Ce fichier n'existe pas !"
			       echo "*************************"
		       fi
		       ;;
	       q)
		       exit 1
		       ;;
	       *)
		       exit 2
		       ;;
       esac
done



