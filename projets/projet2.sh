#!/bin/bash

#*******Projet 2*******

if [ $# -eq 0 ]
then
	echo "Veuillez saisir un nom de groupe" && read -r GROUP
fi
VAR=$GROUP

cut -d ":" -f 1 /etc/group | grep -l "$VAR" > /dev/null

if [ $? -eq 0 ]
then
	echo "Le groupe existe déjà"
else
	echo "Le groupe n'existe pas"
fi

