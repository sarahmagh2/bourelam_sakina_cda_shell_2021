#!/bin/bash

#******Exercice 6***** : Réaliser un compteur qui descend à 1

N=1
echo "Ceci est un compteur par ordre décroissant"
read -p "Entrez votre chiffre : " CHIFFRE

if [ $CHIFFRE -gt 0 ]
then

for     ((i=$CHIFFRE; i>=${N}; i--))
	do
		echo "Décompte :"$i

	done
	echo "*******************"
	echo "Votre décompte est réalisé !"


elif  [ $CHIFFRE -lt 0 ]
then
	let RESULT=-1*$CHIFFRE
for     ((i=$RESULT; i>=${N}; i--))
	do 
		echo "Décompte :" $i	
	done
		echo "***********************"
		echo "Votre décompte est réalisé"
	
else
        echo "Réessayer avec un entier différent de 0"
	exit 0
fi

