#!/bin/bash

#*****Exercice 10 : Ecrire un script qui propose le menu suivant:
                    #************Vérifier l'existence d'un utilisateur
		    #************Connaître l'uid d'un utilisateur
		    #************Quitter

function_saisi_user()

{
	echo "Veuillez saisir l'utilisateur :"
	read -r USER
}

function_verif_user()
{
	VAR=$USER
	grep -l $VAR /etc/passwd 

	if [ $? -eq 0 ]
	then
		echo "L'utilisateur existe"
	else
		echo "L'utilisateur n'existe pas"
		return 2
	fi

}
while :
do 
	echo "1 -Vérifier l'existence d'un utilisateur"
	echo "2 -Connaître l'uid d'un utilisateur"
	echo "q -Quitter"

	read -p "Votre choix :" N

	case $N in 
		1)
			function_saisi_user
			function_verif_user
			;;
		2)
			function_saisi_user
			function_verif_user 
			if [ $? -ne 2 ]
			then
				VAR2=$(id -u $USER)
				echo "Son uid est : "$VAR2
			fi
			;;
		q)
			exit 0
			;;
	esac
done

